import { config } from "../config"
import Browser from "webextension-polyfill"
import { error, info } from "./utils"
import { default as defaults } from "lodash.defaultsdeep"
import { default as lodashGet } from "lodash.get"
import { default as lodashSet } from "lodash.set"
import { default as lodashClone } from "lodash.clonedeep"
import { Ok, Result } from "ts-results"

/**
 * @summary Clear all settings from browser storage
 * @returns
 */
export const clear = async (): Promise<Result<void, Error>> => {
  try {
    await Browser.storage.local.remove("settings")

    return Ok.EMPTY
  } catch (e: any) {
    return error(e)
  }
}

/**
 * @summary Retrieve a specific setting from browser storage
 * @param key the key of the setting to retrieve from browser storage
 * @returns
 */
export const get = async (key: string): Promise<Result<any, Error>> => {
  try {
    const loadResult = await load()

    if (loadResult.ok) {
      return new Ok(lodashGet(loadResult.val, key))
    } else {
      return loadResult
    }
  } catch (e: any) {
    return error(e)
  }
}

/**
 * @summary Initialize settings in browser storage
 * @returns
 */
const init = async (): Promise<Result<any, Error>> => {
  const saveResult = await save(lodashClone(config))

  if (saveResult.ok) {
    return new Ok(lodashClone(config))
  } else {
    return saveResult
  }
}

/**
 * @summary Load settings from browser storage
 * @returns
 */
export const load = async (): Promise<Result<any, Error>> => {
  try {
    const store = await Browser.storage.local.get("settings")

    if (store.settings) {
      return new Ok(defaults(store.settings, lodashClone(config)))
    } else {
      info("Settings not found. Settings will be initialized from default")

      return await init()
    }
  } catch (e: any) {
    return error(e)
  }
}

/**
 * @summary Reset to default all settings from browser storage
 * @returns default settings
 */
export const reset = async (): Promise<Result<any, Error>> => {
  const initResult = await init()

  if (initResult.ok) {
    info("Settings have been reset to default")
    return new Ok(initResult.val)
  } else {
    return error(new Error("Unable to reset settings"))
  }
}

/**
 * @summary Save settings in the browser storage
 * @param settingsObject the whole settings object
 * @returns
 */
export const save = async (settingsObject: any): Promise<Result<void, Error>> => {
  try {
    await Browser.storage.local.set({ settings: settingsObject })

    info("Settings have been saved")
    return Ok.EMPTY
  } catch (e: any) {
    return error(e)
  }
}

/**
 * @summary Add or update a specific setting
 * @param key the key of the setting to add or update
 * @param value the value of the setting to add or update
 * @returns
 */
export const set = async (key: string, value: any): Promise<Result<void, Error>> => {
  try {
    const loadResult = await load()

    if (loadResult.ok) {
      lodashSet(loadResult.val, key, value)

      await Browser.storage.local.set({ settings: loadResult.val })
      info(`The setting ${key} has been saved`)
      return Ok.EMPTY
    } else {
      return loadResult
    }
  } catch (e: any) {
    return error(e)
  }
}
