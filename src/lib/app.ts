import * as Settings from "./settings"
import { error, isEnabled, sendMessageToRuntime } from "./utils"
import { BROWSER_MESSAGE } from "./enums/browser-message"
import type { Result } from "ts-results"

/**
 * @summary Toggle on/off the app
 * @returns
 */
export const toggle = async (): Promise<Result<void, Error>> =>
  (await isEnabled()) ? await disable() : await enable()

/**
 * @summary Disable the app
 * @returns
 */
export const disable = async (): Promise<Result<void, Error>> => {
  try {
    const setResult = await Settings.set("app.isEnabled", false)

    if (setResult.ok) {
      return sendMessageToRuntime(BROWSER_MESSAGE.APP_DISABLE)
    } else {
      return setResult
    }
  } catch (e: any) {
    return error(e)
  }
}

/**
 * Enable the app
 * @returns
 */
export const enable = async (): Promise<Result<void, Error>> => {
  try {
    const setResult = await Settings.set("app.isEnabled", true)

    if (setResult.ok) {
      return sendMessageToRuntime(BROWSER_MESSAGE.APP_ENABLE)
    } else {
      return setResult
    }
  } catch (e: any) {
    return error(e)
  }
}
