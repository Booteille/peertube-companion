/**
 * @summary The type of log entry (so we use the appropriate console method)
 */
export enum LOG_TYPE {
  ERROR,
  INFO,
  WARNING,
}
