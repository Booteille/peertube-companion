/**
 * @summary Multiple APIs exist to fetch YouTube content
 */
export enum REDIRECTOR {
  YOUTUBE,
  INVIDIOUS,
  PIPED,
}
