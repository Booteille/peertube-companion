import type { REDIRECTION_RULES } from "../enums/redirection-rules"
import type { SERVICE } from "../enums/service"

export interface RuleInterface {
  name: REDIRECTION_RULES
  enabled: boolean
  scopes: SERVICE[]
}
