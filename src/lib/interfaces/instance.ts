import type { HostInterface } from "./host"

export interface InstanceInterface extends HostInterface {
  ping: number
}
