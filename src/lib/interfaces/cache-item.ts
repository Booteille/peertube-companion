/**
 * @summary Objects stored in cache must follow this interface
 */
export interface CacheItemInterface {
  data: any
  timestamp: number
  ttl: number
}
