export interface VideoInterface {
  url: string
  uuid: string
  name?: string
}
