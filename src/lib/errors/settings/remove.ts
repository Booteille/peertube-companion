export class SettingsRemoveError extends Error {
  message = "An error occured while trying to remove settings from browser storage"
}
