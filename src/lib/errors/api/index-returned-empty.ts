export class ApiIndexReturnedEmpty extends Error {
  message = "The list of instances returned by the index is empty"
}
