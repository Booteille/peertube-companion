import * as Redirection from "./lib/redirection"
import * as Settings from "./lib/settings"
import { error } from "./lib/utils"
import Browser from "webextension-polyfill"
import { BROWSER_MESSAGE } from "./lib/enums/browser-message"
import * as ExtensionUpdater from "./extension-updater"
import { REDIRECTION_MODE } from "./lib/enums/redirection-mode"

/**
 * @summary Run background tasks such as handling redirection.
 *          This file is the entry point for background scripts defined in manifest.json
 */
const runBackgroundTasks = async () => {
  try {
    // Handle extension update
    const handleInstallResult = ExtensionUpdater.handleInstall()

    if (handleInstallResult.err) {
      throw handleInstallResult.val
    }

    // Handle Redirection
    const settingsResult = await Settings.load()

    if (settingsResult.ok) {
      if (settingsResult.val.redirection.mode === REDIRECTION_MODE.AUTOMATIC) {
        const enableResult = await Redirection.enableBackgroundRedirection()
        if (enableResult.err) {
          enableResult
        }
      }

      Browser.runtime.onMessage.addListener(async (message) => {
        switch (message) {
          case BROWSER_MESSAGE.BACKGROUND_REDIRECTION_DISABLE:
          case BROWSER_MESSAGE.APP_DISABLE:
            Redirection.disableBackgroundRedirection()

            break
          case BROWSER_MESSAGE.BACKGROUND_REDIRECTION_ENABLE:
          case BROWSER_MESSAGE.APP_ENABLE:
            await Redirection.enableBackgroundRedirection()

            break
        }
      })
    } else {
      settingsResult
    }
  } catch (e: any) {
    error(e)
  }
}

runBackgroundTasks()
