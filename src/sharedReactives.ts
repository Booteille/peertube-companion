import { reactive } from "vue"
import { config } from "./config"
import { default as lodashClone } from "lodash.clonedeep"

export const appState = reactive({ settings: lodashClone(config) })
