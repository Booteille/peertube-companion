import { createApp } from "vue"
import App from "./components/Notification.vue"

const el = document.createElement("div")
el.id = "ptc-notification"
document.body.append(el)

createApp(App).mount(`#${el.id}`)
