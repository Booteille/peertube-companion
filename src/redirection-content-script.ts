import { REDIRECTION_MODE } from "./lib/enums/redirection-mode"
import * as Redirection from "./lib/redirection"
import * as Settings from "./lib/settings"
import { error } from "./lib/utils"
import { Ok, Result } from "ts-results"

const handleRedirection = async (): Promise<void> => {
  try {
    const settingsResult = await Settings.load()

    if (settingsResult.ok) {
      const settings = settingsResult.val
      const url = new URL(location.href)

      if (settings.app.isEnabled) {
        if (settings.redirection.mode === REDIRECTION_MODE.AUTOMATIC) {
          redirect(url)
        } else {
          showNotification(url)
        }
      }
    } else {
      settingsResult
    }
  } catch (e: any) {
    error(e)
  }
}

/**
 * Try to get the PeerTube version of the video
 * @param url
 * @returns
 */
const getPeerTubeUrl = async (url: URL): Promise<Result<URL | void, Error>> => {
  try {
    if (await Redirection.isAllowedUrl(url)) {
      const redirectionUrlResult = await Redirection.generateTargetUrl(url)
      if (redirectionUrlResult.ok) {
        if (redirectionUrlResult.val && redirectionUrlResult.val.url) {
          return new Ok(new URL(redirectionUrlResult.val.url))
        }

        return Ok.EMPTY
      } else {
        return redirectionUrlResult
      }
    }

    return Ok.EMPTY
  } catch (e: any) {
    return error(e)
  }
}

/**
 * Redirect to PeerTube if video exists on it
 * @param url The YT url
 */
const redirect = async (url: URL): Promise<Result<void, Error>> => {
  try {
    const redirectionUrlResult = await getPeerTubeUrl(url)

    if (redirectionUrlResult.ok) {
      if (redirectionUrlResult.val) {
        location.replace(redirectionUrlResult.val)
      }

      return Ok.EMPTY
    }

    return redirectionUrlResult
  } catch (e: any) {
    return error(e)
  }
}

/**
 * Show a notification with a button to redirect to PT
 * @param url
 * @returns
 */
const showNotification = async (url: URL): Promise<Result<void, Error>> => {
  try {
    const notification = document.getElementById("ptc-notification-container")
    if (notification) {
      const redirectionUrlResult = await getPeerTubeUrl(url)
      const watchLink = document.getElementById("ptc-notification-watch") as HTMLAnchorElement

      if (redirectionUrlResult.ok && redirectionUrlResult.val && watchLink) {
        watchLink.href = redirectionUrlResult.val.href
        notification.style.display = "flex"
      }
    }
    return Ok.EMPTY
  } catch (e: any) {
    return error(e)
  }
}

// Redirect on page load, if background script missed it
handleRedirection()

// Handle navigation through YT interface
let oldHref = document.location.href
const body = document.body
if (body !== null) {
  const observer = new MutationObserver(() => {
    if (oldHref !== document.location.href) {
      oldHref = document.location.href

      const notification = document.getElementById("ptc-notification-container")

      if (notification) {
        notification.style.display = "none"
      }
      handleRedirection()
    }
  })
  observer.observe(body, { childList: true, subtree: true })
}
