import { createApp } from "vue"
import App from "./components/Popup.vue"
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome"

document.documentElement.lang = navigator.language

createApp(App).component("font-awesome-icon", FontAwesomeIcon).mount("#app")
