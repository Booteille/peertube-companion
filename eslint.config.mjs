import typescriptEslint from "@typescript-eslint/eslint-plugin"
import parser from "vue-eslint-parser"
import path from "node:path"
import { fileURLToPath } from "node:url"
import js from "@eslint/js"
import { FlatCompat } from "@eslint/eslintrc"

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)
const compat = new FlatCompat({
  baseDirectory: __dirname,
  recommendedConfig: js.configs.recommended,
  allConfig: js.configs.all,
})

export default [
  {
    ignores: ["**/node_modules/", "**/web-ext-config.cjs", "**/dist"],
  },
  ...compat.extends(
    "eslint:recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:vue/vue3-recommended",
    "@vue/eslint-config-prettier",
  ),
  {
    plugins: {
      "@typescript-eslint": typescriptEslint,
    },

    languageOptions: {
      parser: parser,
      ecmaVersion: 2020,
      sourceType: "script",

      parserOptions: {
        parser: "@typescript-eslint/parser",
        project: ["./tsconfig.json"],
        extraFileExtensions: [".vue"],
      },
    },

    rules: {
      "eol-last": ["error", "always"],
      "indent": "off",
      "no-lone-blocks": "off",
      "no-mixed-operators": "off",

      "max-len": [
        "warn",
        {
          code: 120,
        },
      ],

      "quote-props": ["error", "consistent-as-needed"],
      "padded-blocks": "off",
      "prefer-regex-literals": "off",
      "no-async-promise-executor": "off",
      "dot-notation": "off",
      "promise/param-names": "off",
      "import/first": "off",

      "operator-linebreak": [
        "error",
        "after",
        {
          overrides: {
            "?": "before",
            ":": "before",
          },
        },
      ],

      "@typescript-eslint/indent": [
        "error",
        2,
        {
          SwitchCase: 1,
          MemberExpression: "off",
        },
      ],

      "@typescript-eslint/consistent-type-assertions": [
        "error",
        {
          assertionStyle: "as",
        },
      ],

      "@typescript-eslint/array-type": [
        "error",
        {
          default: "array",
        },
      ],

      "@typescript-eslint/restrict-template-expressions": [
        "off",
        {
          allowNumber: true,
        },
      ],

      "@typescript-eslint/no-this-alias": [
        "error",
        {
          allowDestructuring: true,
          allowedNames: ["self"],
        },
      ],

      "@typescript-eslint/return-await": "off",
      "@typescript-eslint/dot-notation": "off",
      "@typescript-eslint/method-signature-style": "off",
      "@typescript-eslint/no-base-to-string": "off",
      "@typescript-eslint/quotes": "off",
      "@typescript-eslint/no-var-requires": "off",
      "@typescript-eslint/explicit-function-return-type": "off",
      "@typescript-eslint/promise-function-async": "off",
      "@typescript-eslint/no-dynamic-delete": "off",
      "@typescript-eslint/no-unnecessary-boolean-literal-compare": "off",
      "@typescript-eslint/strict-boolean-expressions": "off",
      "@typescript-eslint/consistent-type-definitions": "off",
      "@typescript-eslint/no-misused-promises": "off",
      "@typescript-eslint/no-namespace": "off",
      "@typescript-eslint/no-empty-interface": "off",
      "@typescript-eslint/no-extraneous-class": "off",
      "@typescript-eslint/no-use-before-define": "off",
      "@typescript-eslint/no-empty-function": "off",
      "require-await": "off",
      "@typescript-eslint/require-await": "error",
      "vue/multi-word-component-names": "off",
      "vue/require-default-prop": "off",
      "vue/no-v-html": "off",
      "@typescript-eslint/no-explicit-any": "off",
      "@typescript-eslint/restrict-plus-operands": "off",
    },
  },
]
